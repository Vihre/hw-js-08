// 1

let elemsOne = document.querySelectorAll('p');
for (let i = 0; i < elemsOne.length; i++) {
    elemsOne[i].style.backgroundColor = "#ff0000";
}

// 2 
let elem = document.getElementById('optionsList')
console.log(elem);
let elemParent = elem.parentNode;
console.log(elemParent);
let elemChildren = elem.childNodes;

for (let elem of elemChildren) {
    console.log(`Ім'я - ${elem.nodeName} Тип-${elem.nodeType}`);
}


// 3 
let textElement = document.querySelector('#testParagraph');
let textElementContent = textElement.textContent;
textElement.textContent = `This is a paragraph`;
console.log(textElement.textContent);

// 4 
let mainChildren = document.querySelectorAll('.main-header .main-nav-item');
console.log(mainChildren);
for (let elem of mainChildren) {
    elem.classList.add('nav-item');
}
console.log(mainChildren);

// 5 
let elemSectionTitle = document.querySelectorAll('.section-title');
for (let elem of elemSectionTitle) {
    elem.classList.remove('section-title')
}
console.log(elemSectionTitle);